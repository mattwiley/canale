from canale.utils.dict_path import value_at_path


INPUT_DICT = dict(
    channel=dict(
        name="PBS Spacetime",
        url="https://youtube.com/sadfadlsfjksaldkfjlk",
        added_at=1234215314,
        metadata=dict(data1="qwer", data2="sdfa"),
    )
)


def test_exception_raised_on_nonetype_input_dict():
    try:
        value_at_path(None, "a.b.c")
    except Exception as e:
        assert str(e) == "No input dictionary provided."


def test_exception_raised_on_nonetype_key_path():
    try:
        value_at_path(INPUT_DICT, None)
    except Exception as e:
        assert str(e) == "Invalid key path value provided."


def test_exception_raised_on_zero_length_key_path():
    try:
        value_at_path(INPUT_DICT, "")
    except Exception as e:
        assert str(e) == "Invalid key path value provided."


def test_exception_raised_on_too_many_segments():
    try:
        value_at_path(INPUT_DICT, "channel.name.subname")
    except Exception as e:
        assert (
            str(e)
            == "Expected dict type at segment 'name', but found type: <class 'str'>"
        )


def test_returns_dict_value():
    expected_result = INPUT_DICT.get("channel").get("metadata")

    actual_result = value_at_path(INPUT_DICT, "channel.metadata")
    assert actual_result == expected_result


def test_returns_str_value():
    expected_result_1 = INPUT_DICT.get("channel").get("name")
    expected_result_2 = INPUT_DICT.get("channel").get("metadata").get("data1")

    actual_result_1 = value_at_path(INPUT_DICT, "channel.name")
    assert actual_result_1 == expected_result_1

    actual_result_2 = value_at_path(INPUT_DICT, "channel.metadata.data1")
    assert actual_result_2 == expected_result_2


def test_returns_int_value():
    expected_result = INPUT_DICT.get("channel").get("added_at")

    actual_result = value_at_path(INPUT_DICT, "channel.added_at")
    assert actual_result == expected_result
