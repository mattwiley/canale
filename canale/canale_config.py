import yaml

from canale.utils.dict_path import value_at_path

class CanaleConfig:
    def __init__(self, config_file_path: str = None) -> None:
        self._config_file_path = config_file_path
        if self._config_file_path is not None:
            with open(self._config_file_path) as config_file:
                config_file_contents = config_file.read()
                self._user_config = yaml.load_all(config_file_contents)

    @property
    def datastore_type(self):
        user_data_type = value_at_path(self._user_config,'datastore.type')
        return user_data_type if user_data_type else CanaleConfig.DEFAULT_DATASTORE_TYPE

    @property
    def datastore_path(self):
        user_data_path = value_at_path(self._user_config,'datastore.path')
        return user_data_path if user_data_path else CanaleConfig.DEFAULT_DATASTORE_PATH

    @staticmethod
    def DEFAULT_DATASTORE_TYPE():
        return "sqlite"

    @staticmethod
    def DEFAULT_DATASTORE_PATH():
        return "~/.config/canale/config.yml"