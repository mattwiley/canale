import os


def dir(file_path:str) -> str:
    return os.path.abspath(os.path.dirname(file_path))