


from typing import Any


def value_at_path(input_dict: dict, key_path: str) -> Any:

    if input_dict is None:
        raise Exception("No input dictionary provided.")
        
    if key_path is None or key_path == '':
        raise Exception("Invalid key path value provided.")

    segments = key_path.split('.')
    value = input_dict.get(segments[0])

    if type(value) == dict:
        if len(segments) > 1:
            return value_at_path(value, '.'.join(segments[1:]))
        else:
            # There are now more segments, assume the dict is the value the user requested
            return value
    elif len(segments) > 1:
        raise Exception(f"Expected dict type at segment '{segments[0]}', but found type: {type(value)}")
    else:
        return value