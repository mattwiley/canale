from canale.models.news_source import NewsSource


class CanaleDataInterface:
    def add_news_source(
        self, url: str, name: str = None, description: str = None
    ) -> NewsSource:
        """Adds a news source to the underlying datastore.

        Args:
            url (str): URL of the target news source (RSS or Atom feed.)
            name (str, optional): Name of the news source. Will attempt to get from feed if not provided. Defaults to None.
            description (str, optional): Description of the news source. Will attempt to get from feed if not provided. Defaults to None.

        Returns:
            NewsSource: The newly created news source.
        """
        pass

    def remove_news_source(self, news_source: NewsSource) -> NewsSource:
        """Removes a news source from the underlying datastore.

        Args:
            news_source (NewsSource): News source to be removed.

        Returns:
            NewsSource: The news source object that has been deleted.

        Raises:
            DataInterfaceOperationException if an error occurs while removing the news source.
        """
        pass
