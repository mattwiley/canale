import sys
import yaml

from argsy import Argsy

from canale.helpers.functions import dir
from canale.cmds.add_feed import AddFeed


def main():

    parsed_args = Argsy(config_file_name=f"{dir(__file__)}/cli_args.yml").parse_args(
        sys.argv[1:], print_result=True
    )

    if parsed_args.get("cmd") == "add":
        AddFeed(parsed_args.get("args")).execute()


if __name__ == "__main__":
    main()
