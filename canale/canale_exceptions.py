class CanaleException(Exception):
    def __init__(self, message) -> None:
        super().__init__(message)


class NewsSourceNotFoundException(CanaleException):
    pass


class DataInterfaceOperationException(BaseException):
    pass
