class NewsSource:
    def __init__(
        self, id: str, url: str, name: str = None, description: str = None
    ) -> None:
        """Creates a new news source objects.

        Args:
            ns_id (str): ID of the news from the underlying datastore.
            url (str): URL of the news source.
            name (str, optional): Name of the news source.. Defaults to None.
            description (str, optional): Description of the news source.. Defaults to None.
        """
        self._id = id
        self._url = url
        self._name = name
        self._description = description
