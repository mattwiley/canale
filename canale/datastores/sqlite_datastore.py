import sqlite3

from typing import Dict, List
from hashlib import sha256
from canale_config import CanaleConfig

from canale_data_interface import CanaleDataInterface
from models.news_source import NewsSource
from canale_exceptions import DataInterfaceOperationException


class Queries:
    @property
    def create_table_news_sources(self):
        return """
            CREATE TABLE IF NOT EXISTS news_source(
                url TEXT UNIQUE NOT NULL,
                name TEXT,
                description TEXT,
                last_modified INTEGER NOT NULL
            );

            CREATE TRIGGER update_news_source_last_modified
                AFTER UPDATE
                ON news_source
                FOR EACH ROW
                WHEN    NEW.name != OLD.name OR
                        NEW.description != OLD.description
            BEGIN
                UPDATE news_source SET last_modified=CURRENT_TIMESTAMP WHERE url=OLD.url;
            END;
        """


class SQLiteDatastore(CanaleDataInterface):
    def __init__(self) -> None:
        super().__init__()
        self._database_file_path = CanaleConfig.DEFAULT_DATASTORE_PATH()

    def _create_table_news_sources(self):
        pass

    def add_news_source(
        self, url: str, name: str = None, description: str = None
    ) -> NewsSource:

        if url is None or len(url) == 0:
            raise DataInterfaceOperationException(
                f"Url is None or empty. Must provide a url when adding a news source."
            )

        try:
            candidate_news_source = NewsSource(
                id=sha256(url.encode("utf-8")).hexdigest(),
                url=url,
                name=name,
                description=description,
            )

            # TODO: Implement

            pass

        except Exception as e:
            raise DataInterfaceOperationException(str(e))

    def remove_news_source(self, news_source: NewsSource) -> NewsSource:
        if news_source is None:
            raise DataInterfaceOperationException(
                f"NewsSource is None. Must provide a NewsSource when adding a news source."
            )

        try:
            # TODO: Implement
            pass
        except Exception as e:
            raise DataInterfaceOperationException(str(e))
